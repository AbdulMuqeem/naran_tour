//
//  PlacesInfoViewModel.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

protocol dataDelegate {
    
    func didRecievedData(data:[InfoDataModel])
    func didRecieveFavorites(favourites:[String])
    func didRecieveRatings(ratings:[Dictionary<String,Any>])
    func didFailed()
}

class InfoViewModel {
    
    let imagesArray = [
        "Maisonette Hotels" : [#imageLiteral(resourceName: "Maisonette Hotels1"),#imageLiteral(resourceName: "Maisonette Hotels2") , #imageLiteral(resourceName: "Maisonette Hotels3"),#imageLiteral(resourceName: "Maisonette Hotels4")],
        "Pakistan Hotel" : [#imageLiteral(resourceName: "Pakistan Hotel")],
        "Millennium Inn Hotel" : [#imageLiteral(resourceName: "Millennium Inn Hotel1"),#imageLiteral(resourceName: "Millennium Inn Hotel2") , #imageLiteral(resourceName: "Millennium Inn Hotel3"),#imageLiteral(resourceName: "Millennium Inn Hotel4")],
        "HOTEL deMANCHI" : [#imageLiteral(resourceName: "HOTEL deMANCHI1"),#imageLiteral(resourceName: "HOTEL deMANCHI2") , #imageLiteral(resourceName: "HOTEL deMANCHI3")],
        "Kunhar View Hotel" : [#imageLiteral(resourceName: "Kunhar View Hotel1"),#imageLiteral(resourceName: "Kunhar View Hotel2") , #imageLiteral(resourceName: "Kunhar View Hotel3")],
        "Sayyam Heights" : [#imageLiteral(resourceName: "Sayyam Heights1"),#imageLiteral(resourceName: "Sayyam Heights2") , #imageLiteral(resourceName: "Sayyam Heights3"),#imageLiteral(resourceName: "Sayyam Heights4")],
        "Faran Hotel Naran" : [#imageLiteral(resourceName: "Faran Hotel Naran1"),#imageLiteral(resourceName: "Faran Hotel Naran2") , #imageLiteral(resourceName: "Faran Hotel Naran3")],
        "Pine Park Restaurant"  : [#imageLiteral(resourceName: "Pine Park Restaurant1"),#imageLiteral(resourceName: "Pine Park Restaurant2") , #imageLiteral(resourceName: "Pine Park Restaurant3")] ,
        "The Troutland Restaurant"  : [#imageLiteral(resourceName: "The Troutland Restaurant1"),#imageLiteral(resourceName: "The Troutland Restaurant2") , #imageLiteral(resourceName: "The Troutland Restaurant3")] ,
        "Gateway Restaurant"  : [#imageLiteral(resourceName: "Gateway Restaurant1"),#imageLiteral(resourceName: "Gateway Restaurant2") , #imageLiteral(resourceName: "Gateway Restaurant3")] ,
        "Moon Restaurant" : [#imageLiteral(resourceName: "Moon Restaurant1"),#imageLiteral(resourceName: "Moon Restaurant2") , #imageLiteral(resourceName: "Moon Restaurant3")] ,
        "Royal Dream Restaurant" : [#imageLiteral(resourceName: "Royal Dream Restaurant1"),#imageLiteral(resourceName: "Royal Dream Restaurant2") , #imageLiteral(resourceName: "Royal Dream Restaurant3")] ,
        "Lala Waterfall" : [#imageLiteral(resourceName: "Lala Waterfall1"),#imageLiteral(resourceName: "Lala Waterfall2") , #imageLiteral(resourceName: "Lala Waterfall3")] ,
        "Sabri Waterfall" : [#imageLiteral(resourceName: "Sabri Waterfall1"),#imageLiteral(resourceName: "Sabri Waterfall2") , #imageLiteral(resourceName: "Sabri Waterfall3")] ,
        "Lalazar Fall" : [#imageLiteral(resourceName: "Lalazar Fall1"),#imageLiteral(resourceName: "Lalazar Fall2")] ,
        "Naran Waterfall" : [#imageLiteral(resourceName: "Naran Waterfall1"),#imageLiteral(resourceName: "Naran Waterfall2") , #imageLiteral(resourceName: "Naran Waterfall3")] ,
        "Lake Saiful Muluk" : [#imageLiteral(resourceName: "Lake Saiful Muluk1"),#imageLiteral(resourceName: "Lake Saiful Muluk2") , #imageLiteral(resourceName: "Lake Saiful Muluk3")] ,
        "Lulusar Lake" : [#imageLiteral(resourceName: "Lulusar Lake1"),#imageLiteral(resourceName: "Lulusar Lake2") , #imageLiteral(resourceName: "Lulusar Lake3")] ,
        "Ansoo Lake" : [#imageLiteral(resourceName: "Ansoo Lake1"),#imageLiteral(resourceName: "Ansoo Lake2") , #imageLiteral(resourceName: "Ansoo Lake3")] ,
        "Dudipatsar Lake" : [#imageLiteral(resourceName: "Dudipatsar Lake1"),#imageLiteral(resourceName: "Dudipatsar Lake2") , #imageLiteral(resourceName: "Dudipatsar Lake3")] ,
        "Pyala Lake" : [#imageLiteral(resourceName: "Pyala Lake1"),#imageLiteral(resourceName: "Pyala Lake2") , #imageLiteral(resourceName: "Pyala Lake3")]
    ]
    
    var delegate : dataDelegate?
    var ratingDelegate : RatingsDelegate?
    
    //MARK:- Hotels Fetch
    
    func fetchHotelsData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("hotels").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    //MARK:- Restaurants Fetch
    
    func fetchRestaurantsData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("restaurants").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    //MARK:- WaterFall Fetch
    
    func fetchWaterFallsData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("waterFall").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    //MARK:- Lakes Fetch
    
    func fetchLakesData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("lakes").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    func getFavourites() {
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            return
        }
        
        var favourites = [String]()
        let ref = Database.database().reference()
        ref.child("favorites").observeSingleEvent(of: .value) { (snapshot) in
            
            if let dataDic = snapshot.value as? Dictionary<String,Any>
            {
                for singleData in dataDic
                {
                    if singleData.key == userid
                    {
                        let data = singleData.value as? Dictionary<String,String>
                        
                        for item in data! {
                            if singleData.key == userid {
                                favourites.append(item.key)

                            }

                        }
                    }
                }
                self.delegate?.didRecieveFavorites(favourites: favourites)
            }
        }
    }
    
    
    func getRatings(place:String) {
        
        let ref = Database.database().reference()
        ref.child("ratings").observeSingleEvent(of: .value) { (snapshot) in
            
            if let dataDic = snapshot.value as? Dictionary<String,Any>
            {
                for singleData in dataDic
                {
                    var one : Double = 0
                    var two : Double = 0
                    var three : Double = 0
                    var four : Double = 0
                    var five : Double = 0
                    
                    if singleData.key == place
                    {
                        let data = singleData.value as? Dictionary<String,Any>
                        
                        for item in data!
                        {
                            if let singleRating = item.value as? Double
                            {
                                if singleRating == 1
                                {
                                    one = one + 1
                                }
                                else if singleRating == 2
                                {
                                    two = two + 1
                                }
                                else if singleRating == 3
                                {
                                    three = three + 1
                                }
                                else if singleRating == 4
                                {
                                    four = four + 1
                                }
                                else if singleRating == 5
                                {
                                    five = five + 1
                                }
                            }
                        }
                        
                        let rate = (one*1)+(two*2)+(three*3)+(four*4)+(five*5)
                        let total = (one+two+three+four+five)
                        
                        if total == 0.0 {
                            self.ratingDelegate?.didRecieveRatings(ratings: 0.0)
                            return
                        }
                        
                        let rating : Double = rate/total
                        self.ratingDelegate?.didRecieveRatings(ratings: rating)
                        break
                    }
                }
                
            }
        }
    }
    
    func getMyRatings() {
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            return
        }
        
        var ratingsList = [Dictionary<String,Any>]()
            let ref = Database.database().reference()
            ref.child("ratings").observeSingleEvent(of: .value) { (snapshot) in
                
                // Name & rating
                if let dataDic = snapshot.value as? Dictionary<String,Any> {
                    
                    for singleData in dataDic {
                        
                            let data = singleData.value as? Dictionary<String,Any>
                        
                            for item in data!
                            {
                                if item.key == userid
                                {
                                   var dic = Dictionary<String,Any>()
                                    dic["placeName"] = singleData.key
                                    dic["ratings"] = item.value
                                    ratingsList.append(dic)
                                }
                                
                            }
                        
                    }
                    self.delegate?.didRecieveRatings(ratings: ratingsList)
                }
            }
        }
        
    }
    
    struct InfoDataModel {
        
        var placeName: String?
        var placeDetail: String?
        var placeImages: [UIImage]?
        var placeLocation: String?
        var isFavourites: String?
        var ratings: String?
        
        init(placeName:String,placeDetail:String,placeImages:[UIImage],placeLocation:String,isFavourites:String,ratings:String)
        {
            self.placeName = placeName
            self.placeDetail = placeDetail
            self.placeImages = placeImages
            self.placeLocation = placeLocation
            self.isFavourites = isFavourites
            self.ratings = ratings
        }
}

