//
//  Constants.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

public let THEME_COLOR: UInt = 0x608DBC

public let BACK_IMAGE: UIImage = UIImage(named:"back_icon")!
public let MENU_IMAGE: UIImage = UIImage(named:"menu_icon")!
public let SHARE_IMAGE: UIImage = UIImage(named:"share_icon")!

// Alert
public let SUCCESS_IMAGE: UIImage = UIImage(named:"success")!
public let FAILURE_IMAGE: UIImage = UIImage(named:"error")!

let USERUPDATED = "UserUpdated"
var isGUEST:Bool = false
var TYPE:String = ""


let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

// Extra

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"
public let NOTIFICATION_USERUPDATE = "user_update"

