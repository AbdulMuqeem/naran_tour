//
//  AppDelegate.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 06/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        IQKeyboardManager.shared.enable = true
        UIApplication.shared.statusBarStyle = .lightContent
        FirebaseApp.configure()
        
        self.NavigateTOInitialViewController()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func NavigateTOInitialViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        if Auth.auth().currentUser == nil {
            
            let nav = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = nav
            let vc = LoginViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)

        }
        else {
            
            if let id = Auth.auth().currentUser?.uid {
                 UserDefaults.standard.set(id, forKey: "userid")
            }
           
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            AppDelegate.getInstatnce().window?.rootViewController = controller
            
        }
        
    }
    
}

