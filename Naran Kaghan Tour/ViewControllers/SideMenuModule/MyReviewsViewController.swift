//
//  MyReviewsViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension MyReviewsViewController : dataDelegate {
 
    
    func didRecieveRatings(ratings: [Dictionary<String, Any>]) {
        self.myRatingsArray = ratings
        
        if self.myRatingsArray.count == 0 {
            self.showBanner(title: "Alert", subTitle: "No Ratings Found", style: .warning)
            self.stopLoading()
            return
        }
        self.tblView.reloadData()
        self.stopLoading()
    }
    
    
    func didRecievedData(data: [InfoDataModel]) {
        
    }
    
    func didRecieveFavorites(favourites: [String]) {
        
    }
    
    
    func didFailed() {
        
    }
    
    
}

class MyReviewsViewController: UIViewController {

    class func instantiateFromStoryboard() -> MyReviewsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyReviewsViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    let dataModel = InfoViewModel()
    var dataArray = [InfoDataModel]()
    var myRatingsArray = [Dictionary<String,Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Ratings"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:MyReviewsCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: MyReviewsCell.self))
        
        self.startLoading(message: "")
        self.dataModel.delegate = self
        self.dataModel.getMyRatings()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }

}

extension MyReviewsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myRatingsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(80)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = self.myRatingsArray[indexPath.row]
        
        let cell:MyReviewsCell = tableView.dequeueReusableCell(withIdentifier:String(describing:MyReviewsCell.self)) as!  MyReviewsCell
        
        if let title = obj["placeName"] as? String {
            cell.lblTitle.text = title
        }
        
        if let rating = obj["ratings"] as? Double {
            cell.rating.rating = rating
        }
        
        return cell
    }

}
