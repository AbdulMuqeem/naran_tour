//
//  SettingViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SettingViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SettingViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func action(_ sender : UIButton) {
        
        if sender.tag == 1 {
            let vc = AboutUsViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 2 {
            UIApplication.shared.open(URL(string: "https://www.termsfeed.com/privacy-policy/bf65f86c3de5eefe4cc8a17470279b2d")!)
        }
        else if sender.tag == 3 {
            if isGUEST == true {
                self.showBanner(title: "Alert", subTitle: "kindly Login to access this feature", style: .warning)
            }
            else {
                let vc = FeedbackViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
    }
    
}

