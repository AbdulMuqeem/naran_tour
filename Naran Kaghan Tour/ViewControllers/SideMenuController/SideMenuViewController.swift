//
//  SideMenuViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblName:UILabel!
    
    var itemsLogin: [String] = [ "Home" , "My Profile" , "My Favourites" , "My Ratings" ,  "Settings" , "Call Us" , "Logout"]
    var imagesLogin: [UIImage] = [UIImage(named:"home_icon")! , UIImage(named:"profile_icon")! , UIImage(named:"favourites_icon")! ,UIImage(named:"reviews_icon")!, UIImage(named:"setting_icon")! ,
                                  UIImage(named: "call_icon")! , UIImage(named:"logout_icon")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:SideMenuTableViewCell.self), bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: String(describing: SideMenuTableViewCell.self))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileUpdate), name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
        
        if isGUEST == true {
            self.lblName.text = "Guest User"
        }
        else {
            let ref = Database.database().reference()
            ref.child("users").observeSingleEvent(of: .value, with:
                { (data) in
                    
                    let user = data.value as? Dictionary<String,Any>
                    if let userData = user?[(Auth.auth().currentUser?.uid)!] as? Dictionary<String,Any>
                    {
                        
                        if let id = Auth.auth().currentUser?.uid {
                            UserDefaults.standard.set(id, forKey: "userid")
                        }
                        
                        if let name = userData["username"] as? String {
                            self.lblName.text = name
                            UserDefaults.standard.set(name, forKey: "username")
                        }
                        
                        if let email = userData["email"] as? String {
                            UserDefaults.standard.set(email, forKey: "email")
                        }
                        
                    }
                    
            })
        }
    }
    
    @objc func getProfileUpdate() {
    
        let ref = Database.database().reference()
        ref.child("users").observeSingleEvent(of: .value, with:
            { (data) in
                
                let user = data.value as? Dictionary<String,Any>
                if let userData = user?[(Auth.auth().currentUser?.uid)!] as? Dictionary<String,Any>
                {
                    
                    if let id = Auth.auth().currentUser?.uid {
                        UserDefaults.standard.set(id, forKey: "userid")
                    }
                    
                    if let name = userData["username"] as? String {
                        self.lblName.text = name
                        UserDefaults.standard.set(name, forKey: "username")
                    }
                    
                    if let email = userData["email"] as? String {
                        UserDefaults.standard.set(email, forKey: "email")
                    }
                    
                }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func signout() {
        do {
            try Auth.auth().signOut()
            
            let nav = RootViewController.instantiateFromStoryboard()
            AppDelegate.getInstatnce().window?.rootViewController = nav
            let vc = LoginViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
            
            UserDefaults.standard.set(nil, forKey: "userid")
            UserDefaults.standard.set(nil, forKey: "username")
            UserDefaults.standard.set(nil, forKey: "email")
        }
        catch let error {
            self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
        }
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsLogin.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(70)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        
        cell.lblTitle.text = self.itemsLogin[indexPath.row]
        cell.imgView.image = self.imagesLogin[indexPath.row]
        
        if isGUEST == true {
            if indexPath.row == 6 {
                cell.lblTitle.text = "Login"
            }
        }
        
        if indexPath.row == 6 {
            cell.lineView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
        }
        else if indexPath.row == 1 {
            if isGUEST == true {
                self.showBanner(title: "Alert", subTitle: "kindly Login to access this feature", style: .warning)
            }
            else {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = MyProfileViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
            }
        }
        else if indexPath.row == 2 {
            if isGUEST == true {
                self.showBanner(title: "Alert", subTitle: "kindly Login to access this feature", style: .warning)
            }
            else {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = MyFavouritesViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
                
            }
        }
        else if indexPath.row == 3 {
            if isGUEST == true {
                self.showBanner(title: "Alert", subTitle: "kindly Login to access this feature", style: .warning)
            }
            else {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = MyReviewsViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
                
            }
        }
        else if indexPath.row == 4 {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = SettingViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
        }
        else if indexPath.row == 5 {
            DispatchQueue.main.async {
                UIApplication.shared.openURL(NSURL(string: "tel://923358112478")! as URL)
            }
        }
        else if indexPath.row == 6 {
            
            if isGUEST == true {
                let nav = RootViewController.instantiateFromStoryboard()
                AppDelegate.getInstatnce().window?.rootViewController = nav
                let vc = LoginViewController.instantiateFromStoryboard()
                nav.pushViewController(vc, animated: true)
                return
            }
            
            self.signout()
            
        }
        
    }
    
}














