//
//  HomeDetailViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeDetailViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    var type:String! = ""
    
    let dataModel = InfoViewModel()
    
    var dataArray = [InfoDataModel]()
    var allData = [InfoDataModel]()
    
    var favorites = [String]()
    var isFavorites = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = type
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.dataModel.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:HomeDetailCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: HomeDetailCell.self))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startLoading(message: "")
        self.dataModel.getFavourites()
        
        if self.type == "Hotels" {
            self.startLoading(message: "")
            self.dataModel.fetchHotelsData()
        }
        else if self.type == "Restaurants" {
            self.startLoading(message: "")
            self.dataModel.fetchRestaurantsData()
        }
        else if self.type == "Water Falls" {
            self.startLoading(message: "")
            self.dataModel.fetchWaterFallsData()
        }
        else if self.type == "Lakes" {
            self.startLoading(message: "")
            self.dataModel.fetchLakesData()
        }
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension HomeDetailViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(310)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj:InfoDataModel! = self.dataArray[indexPath.row]
        
        let cell:HomeDetailCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeDetailCell.self)) as!  HomeDetailCell
        
        if let title = obj.placeName {
            cell.lblTitle.text = title
        }
        
        if let description = obj.placeDetail {
            cell.lblDescription.text = description
        }
        
        if let image = obj.placeImages!.first {
            cell.imgBanner.image = image
        }
        
        if let ratings = obj.ratings {
            cell.rating.rating = ratings.toDouble()!
        }
        
        cell.imgHeart.isHidden = true
        
//        if isGUEST != true {
//
//            if self.favorites.count != 0 {
//
//                for favs in favorites {
//                        if obj.isFavourites == "true" {
//                            if favs == obj.placeName {
//                            cell.imgHeart.image = UIImage(named: "heart_red")
//                        }
//                    }
//                }
//
//            }
//            else {
//                cell.imgHeart.image = UIImage(named: "favourites_icon")
//            }
//        }
        
        //            if self.favorites.count != 0 {
        //
        //                for fav in self.favorites {
        //
        //                    if fav == obj.placeName {
        //                        cell.imgHeart.image = UIImage(named: "heart_red")
        //                    } else {
        //                        cell.imgHeart.image = UIImage(named: "favourites_icon")
        //                    }
        //                }
        //
        //            } else {
        //                cell.imgHeart.image = UIImage(named: "favourites_icon")
        //            }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj:InfoDataModel! = self.dataArray[indexPath.row]
        
        let vc = DetailViewController.instantiateFromStoryboard()
        vc.type = self.type
        vc.obj = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeDetailViewController: dataDelegate {
    
    func didRecieveRatings(ratings: [Dictionary<String,Any>])
    {
        
    }
    
    
    func didRecievedData(data: [InfoDataModel]) {
        
        let sortedData = data.sorted { $0.placeName! < $1.placeName! }
        self.allData = sortedData
        self.dataArray = sortedData
        self.tblView.reloadData()
        self.stopLoading()
    }
    
    func didRecieveFavorites(favourites: [String]){
        self.favorites = favourites
    }
    
    
    func didFailed() {
        
        self.showBanner(title: "Error", subTitle: "Failed to fetch data", style: .danger)
        self.stopLoading()
    }
    
}






