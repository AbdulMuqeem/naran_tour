//
//  RegisterViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class RegisterViewController: UIViewController {

    class func instantiateFromStoryboard() -> RegisterViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RegisterViewController
    }
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Register"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func registerAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let fullName = self.txtFullName.text!
        let password = self.txtPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        
        if fullName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter full name" , style: .danger)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter password" , style: .danger)
            return
        }
        
        if  confirmPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter confirm password" , style: .danger)
            return
        }
        
        guard let validNewPassword = self.txtPassword.text , AppHelper.isValidPassword(testStr: validNewPassword) else {
            self.showBanner(title: "Error", subTitle: "Please enter minimum 6 digit password" , style: .danger)
            return
        }
        
        if validNewPassword != confirmPassword {
            self.showBanner(title: "Error", subTitle: "Password & confirm password does not match" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        Auth.auth().createUser(withEmail:email, password:validNewPassword) { (result, error) in
            
            if let error = error {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            guard let uid = result?.user.uid else { return }
            
            let params = ["email":email, "username":fullName]
            print("Params: \(params)")
            
            Database.database().reference().child("users").child(uid).updateChildValues(params, withCompletionBlock: { (error, ref) in
                
                if let error = error {
                    self.stopLoading()
                    self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                    return
                }
                
                self.stopLoading()
                
                let controller = SideMenuRootViewController.instantiateFromStoryboard()
                controller.leftViewPresentationStyle = .slideAbove
                isGUEST = false
                TYPE = "Register"
                AppDelegate.getInstatnce().window?.rootViewController = controller
                
            })
            
        }
        
    }
}
