//
//  LoginViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class LoginViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.isNavigationBarHidden = true
    }
    
    @IBAction func loginAction( _ sender: UIButton) {
        
        self.view.endEditing(true)
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        guard let password = self.txtPassword.text, AppHelper.isValidPassword(testStr: password) else {
            self.showBanner(title: "Error", subTitle: "Please enter 6 digit password" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        Auth.auth().signIn(withEmail: email , password: password) { (result, error) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            
            TYPE = "Login"
            isGUEST = false
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            AppDelegate.getInstatnce().window?.rootViewController = controller
        }
    }
    
    @IBAction func registerAction( _ sender: UIButton) {
        self.view.endEditing(true)
        let vc = RegisterViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgotAction( _ sender: UIButton) {
        self.view.endEditing(true)
        let vc = ForgotPasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func guestAction( _ sender: UIButton) {
        self.view.endEditing(true)
        isGUEST = true
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .slideAbove
        AppDelegate.getInstatnce().window?.rootViewController = controller
    }
    
    
}



