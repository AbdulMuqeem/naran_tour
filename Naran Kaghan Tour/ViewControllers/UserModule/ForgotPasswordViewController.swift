//
//  ForgotPasswordViewController.swift
//  Naran Kaghan Tour
//
//  Created by Abdul Muqeem on 21/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ForgotPasswordViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ForgotPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotPasswordViewController
    }
    
    @IBOutlet weak var txtEmail:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Forgot Password"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendAction(_ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        Auth.auth().sendPasswordReset(withEmail:email) { (error) in
            
            if let error = error {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            
            self.ShowAlert(title: "Success", message: "We have just sent you a password reset email. Please check your inbox and follow the instructions to reset your password", OkActionHandler: { (success) in
                
                self.navigationController?.popViewController(animated: true)
                
            })
            
        }
        
    }
}

